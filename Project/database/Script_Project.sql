-- Tabla carrito_de_compras
CREATE TABLE carrito_de_compras (
    IdCliente INT PRIMARY KEY IDENTITY,
    IdProducto VARCHAR(100),
    Cantidad INT,
    FOREIGN KEY (IdCliente) REFERENCES clientes(ClienteId)
);

-- Tabla productos
CREATE TABLE productos (
    IdProducto INT PRIMARY KEY IDENTITY,
    Nombre VARCHAR(100)
);

-- Tabla clientes
CREATE TABLE clientes (
    ClienteId INT PRIMARY KEY IDENTITY,
    Nombres VARCHAR(100),
    Email VARCHAR(100)
);
---------Tarea 1: Consulta SQL con JOIN--------------------------------------------------------------
CREATE VIEW VistaCarritoDeCompras AS
SELECT c.ClienteId AS clientID,
       p.IdProducto AS productoID,
       p.Nombre AS ProductoNombre,
       c.Nombres AS ClienteNombre,
       cc.Cantidad
FROM clientes c
JOIN carrito_de_compras cc ON c.ClienteId = cc.IdCliente
JOIN productos p ON cc.IdProducto = p.IdProducto;

SELECT * FROM VistaCarritoDeCompras;

-------------------Tarea 2: Consultas SQL sin JOIN (cada tabla en una base de datos diferente)----------------------------------------------------
create database clientes_db;
create database productos_db;
create database carrito_db;

CREATE TABLE clientes_db.dbo.clientes (
    ClienteId INT PRIMARY KEY,
    Nombres VARCHAR(100),
    Email VARCHAR(100)
);

CREATE TABLE productos_db.dbo.productos (
    IdProducto INT PRIMARY KEY,
    Nombre VARCHAR(100)
);

CREATE TABLE carrito_db.dbo.carrito_de_compras (
    IdCliente INT,
    IdProducto INT,
    Cantidad INT,
    FOREIGN KEY (IdCliente) REFERENCES clientes_db.dbo.clientes(ClienteId),
    FOREIGN KEY (IdProducto) REFERENCES productos_db.dbo.productos(IdProducto)
);

SELECT c.ClienteId AS clientID,
       p.IdProducto AS productoID,
       p.Nombre AS ProductoNombre,
       c.Nombres AS ClienteNombre,
       cc.Cantidad
FROM clientes_db.dbo.clientes c
JOIN carrito_db.dbo.carrito_de_compras cc ON c.ClienteId = cc.IdCliente
JOIN productos_db.dbo.productos p ON cc.IdProducto = p.IdProducto;
