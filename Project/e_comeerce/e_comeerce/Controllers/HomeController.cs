﻿using e_comeerce.Context;
using e_comeerce.ViewModels;
using Microsoft.AspNetCore.Mvc;


namespace e_comeerce.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly ApplicationContext _applicationContext;

        public HomeController(ApplicationContext applicationContext)
        {
            _applicationContext=applicationContext;
        }
        public ActionResult<IEnumerable<ConsultaViewModels>> GetConsulta()
        {
            var consulta = (from carrito in _applicationContext.CarritoCompras
                            join cliente in _applicationContext.Clientes on carrito.IdCliente equals cliente.ClienteId
                            join producto in _applicationContext.Productos on carrito.IdProducto equals producto.IdProducto
                            select new ConsultaViewModels
                            {
                                clientID = cliente.ClienteId,
                                productoID = producto.IdProducto,
                                ProductoNombre = producto.Nombre,
                                ClienteNombre = cliente.Nombres,
                                Cantidad = carrito.Cantidad
                            }).ToList();

            return Ok(consulta);
        }

        [HttpGet("clientes")]
        public ActionResult<IEnumerable<Cliente>> GetClientes()
        {
            var clientes = _applicationContext.Clientes.ToList();
            return Ok(clientes);
        }

        [HttpGet("carritos")]
        public ActionResult<IEnumerable<CarritoCompra>> GetCarritos()
        {
            var carritos = _applicationContext.CarritoCompras.ToList();
            return Ok(carritos);
        }

        [HttpGet("productos")]
        public ActionResult<IEnumerable<Producto>> GetProductos()
        {
            var productos = _applicationContext.Productos.ToList();
            return Ok(productos);
        }

        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
