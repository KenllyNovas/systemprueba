﻿namespace e_comeerce.Context
{
    public class Cliente
    {
        public Guid ClienteId { get; set; }
        public string Nombres { get; set; }
        public string Email { get; set; }
    }
}
