﻿using Microsoft.EntityFrameworkCore;

namespace e_comeerce.Context
{
    public partial class ApplicationContext : DbContext
    {
        public ApplicationContext()
        {

        }

        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CarritoCompra>().HasNoKey();
            modelBuilder.Entity<Producto>().HasNoKey();
            modelBuilder.Entity<Cliente>().HasNoKey();
        }
        public virtual DbSet<CarritoCompra>  CarritoCompras { get; set; } = null!;
        public virtual DbSet<Producto>  Productos { get; set; } = null!;
        public virtual DbSet<Cliente>  Clientes { get; set; } = null!;

    }
}
