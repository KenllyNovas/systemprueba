﻿namespace e_comeerce.Context
{
    public class CarritoCompra
    {
        public Guid IdCliente { get; set; }
        public Guid IdProducto { get; set; }
        public int Cantidad { get; set; }
    }
}
