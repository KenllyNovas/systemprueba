﻿namespace e_comeerce.ViewModels
{
    public class ConsultaViewModels
    {
        public Guid clientID { get; set; }
        public Guid productoID { get; set; }
        public string ProductoNombre { get; set; }
        public string ClienteNombre { get; set; }
        public int Cantidad { get; set; }
    }
}
