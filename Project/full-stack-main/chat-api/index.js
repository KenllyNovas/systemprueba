const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const port = 3000;

// create application/json parser
var jsonParser = bodyParser.json()

const chats = [{ "chatId": 1, "destinataryId": "ddcc54e5-4e93-4591-a597-135504450601", "destinatary": "obumfrey0", "lastMessage": "2021-08-02T14:59:46Z" },
{ "chatId": 2, "destinataryId": "754eae32-799a-45dd-8b28-8bb9de754dc5", "destinatary": "keat1", "lastMessage": "2021-04-24T18:28:44Z" },
{ "chatId": 3, "destinataryId": "67e0b3b8-0fdc-4591-a229-71a63c6cf827", "destinatary": "khadfield2", "lastMessage": "2022-07-14T13:07:17Z" },
{ "chatId": 4, "destinataryId": "65b71baa-846e-4e23-9e89-cdded137a7ee", "destinatary": "elade3", "lastMessage": "2021-12-01T07:13:39Z" },
{ "chatId": 5, "destinataryId": "ad37d69f-9bc6-4010-a465-ae14cc84763d", "destinatary": "fcoogan4", "lastMessage": "2021-07-15T11:26:49Z" },
{ "chatId": 6, "destinataryId": "4f773541-edd3-4461-9c2b-30165cf7a5ea", "destinatary": "jbeastall5", "lastMessage": "2021-11-13T22:12:28Z" },
{ "chatId": 7, "destinataryId": "cc5dd5be-1ea1-44cb-903f-572f46bf9565", "destinatary": "gminard6", "lastMessage": "2021-08-04T12:54:45Z" },
{ "chatId": 8, "destinataryId": "d9566a4a-56cc-4a5a-8371-3421cda9ab55", "destinatary": "mreihm7", "lastMessage": "2021-08-18T01:00:33Z" },
{ "chatId": 9, "destinataryId": "d4cc5070-8a89-4839-834f-5652f37a052b", "destinatary": "dpepin8", "lastMessage": "2021-03-15T11:55:44Z" },
{ "chatId": 10, "destinataryId": "af5d90ca-4ae5-49b0-80cf-e5bcda8507c7", "destinatary": "ofreckelton9", "lastMessage": "2021-11-02T00:37:14Z" },
{ "chatId": 11, "destinataryId": "f4cba504-2367-4a50-ab0d-cc9f5611553f", "destinatary": "savarda", "lastMessage": "2021-11-05T23:10:30Z" },
{ "chatId": 12, "destinataryId": "2cdd25d2-24b0-498e-90eb-8f12f7b4d7fb", "destinatary": "omaciloryb", "lastMessage": "2022-05-24T06:42:01Z" },
{ "chatId": 13, "destinataryId": "aef3734e-d95f-4f48-874e-5b7c3dd9df42", "destinatary": "nverlingc", "lastMessage": "2022-04-01T04:06:03Z" },
{ "chatId": 14, "destinataryId": "06870237-ce6a-44c9-a682-091ec11de4c1", "destinatary": "idymentd", "lastMessage": "2022-07-11T22:08:33Z" },
{ "chatId": 15, "destinataryId": "8cf5ae4b-0a81-433f-ac69-d1633aae91c8", "destinatary": "bpoppye", "lastMessage": "2021-12-09T13:54:16Z" },
{ "chatId": 16, "destinataryId": "65f20f48-688f-42fa-8057-4bf986ba5065", "destinatary": "aclementsf", "lastMessage": "2021-12-20T16:06:04Z" },
{ "chatId": 17, "destinataryId": "349eba66-5af9-4536-95d5-a9c51a870e4a", "destinatary": "fskelingtong", "lastMessage": "2022-11-28T22:22:12Z" },
{ "chatId": 18, "destinataryId": "bd7f7ba7-3bcd-4914-ba0e-648f76c1b366", "destinatary": "jprimaroloh", "lastMessage": "2021-12-09T11:41:00Z" },
{ "chatId": 19, "destinataryId": "676b20e4-d702-48d1-8573-407596199c01", "destinatary": "mgonnini", "lastMessage": "2022-09-09T08:37:47Z" },
{ "chatId": 20, "destinataryId": "7ee5a2a7-1f4c-4cbf-93f3-a63a41a1a8ea", "destinatary": "agabbetisj", "lastMessage": "2021-11-09T15:03:31Z" },
{ "chatId": 21, "destinataryId": "32835d51-f3bc-41a0-9ae6-d488e5e8cc5d", "destinatary": "rtilzeyk", "lastMessage": "2021-05-28T18:20:03Z" },
{ "chatId": 22, "destinataryId": "5503fedc-5ee1-468f-b500-d86530b7ba2c", "destinatary": "plutzl", "lastMessage": "2021-07-05T22:07:46Z" },
{ "chatId": 23, "destinataryId": "06faa79f-75f3-43cc-9dc2-e4224cb64085", "destinatary": "dsetterm", "lastMessage": "2021-03-24T18:08:15Z" },
{ "chatId": 24, "destinataryId": "1fe1a39a-97c0-4ebd-baad-43dd4c052126", "destinatary": "fbaslern", "lastMessage": "2022-01-10T04:51:42Z" },
{ "chatId": 25, "destinataryId": "5118cd92-77ec-43a5-a123-0278c2111437", "destinatary": "sbacopo", "lastMessage": "2021-02-11T09:18:26Z" },
{ "chatId": 26, "destinataryId": "e32dcd17-6d14-42ac-b8d0-a6ee10c1f2d4", "destinatary": "creddingtonp", "lastMessage": "2022-03-04T10:42:58Z" },
{ "chatId": 27, "destinataryId": "b38e8ea4-0ea4-4f03-aed6-7136a98ae68e", "destinatary": "mshortoq", "lastMessage": "2022-09-13T22:35:05Z" },
{ "chatId": 28, "destinataryId": "fcf11c13-a27f-49eb-a86c-835b3253c323", "destinatary": "miannuzzir", "lastMessage": "2022-08-30T04:21:16Z" },
{ "chatId": 29, "destinataryId": "94cd7737-26cd-4035-b182-be30d728b44c", "destinatary": "fgroartys", "lastMessage": "2022-04-05T21:35:55Z" },
{ "chatId": 30, "destinataryId": "a499eceb-094d-4f51-845a-576e0bda9e60", "destinatary": "cmatist", "lastMessage": "2022-07-11T23:47:57Z" },
{ "chatId": 31, "destinataryId": "4df17afb-1a27-498b-b76d-1921a54b6c48", "destinatary": "apallisu", "lastMessage": "2022-04-15T08:23:37Z" },
{ "chatId": 32, "destinataryId": "4c1d4cd2-4210-40e6-829a-4b19c0c087b5", "destinatary": "codugganv", "lastMessage": "2022-04-28T23:39:55Z" },
{ "chatId": 33, "destinataryId": "f94d2013-486f-4001-a84f-669a3b959f6d", "destinatary": "rbatrickw", "lastMessage": "2022-01-10T11:15:51Z" },
{ "chatId": 34, "destinataryId": "8815fe89-2558-4350-b1e0-40b3b86fe041", "destinatary": "bstorrarx", "lastMessage": "2022-12-18T07:49:14Z" },
{ "chatId": 35, "destinataryId": "caff9313-75fa-4c2d-8f11-b94e2c651d2a", "destinatary": "eabreharty", "lastMessage": "2022-09-13T09:44:09Z" },
{ "chatId": 36, "destinataryId": "2cd4af07-7d35-4ad8-befb-540414b15c7f", "destinatary": "btonbyez", "lastMessage": "2021-09-12T13:36:13Z" },
{ "chatId": 37, "destinataryId": "d07a1c2f-5ae4-4cc0-897c-ca81ced5b148", "destinatary": "aalpes10", "lastMessage": "2022-05-07T16:21:28Z" },
{ "chatId": 38, "destinataryId": "9167c3af-ceee-4c62-a2eb-712376f80fa4", "destinatary": "jhadleigh11", "lastMessage": "2021-04-22T20:04:08Z" },
{ "chatId": 39, "destinataryId": "3fbef619-6991-4bd6-9e7a-276542f6b8c9", "destinatary": "hgout12", "lastMessage": "2021-04-12T04:58:04Z" },
{ "chatId": 40, "destinataryId": "928107c9-3fde-426e-83c7-17c52742c95f", "destinatary": "lwhitmell13", "lastMessage": "2021-01-29T12:36:24Z" },
{ "chatId": 41, "destinataryId": "d76617a5-33e3-44de-baa1-4bd40113497b", "destinatary": "khanley14", "lastMessage": "2022-01-07T16:47:38Z" },
{ "chatId": 42, "destinataryId": "45f9714e-2b8d-41c0-a4df-d332f13e6c21", "destinatary": "adominec15", "lastMessage": "2022-04-10T00:17:49Z" },
{ "chatId": 43, "destinataryId": "031bc7ce-49c2-4817-8dcb-aa783423cddf", "destinatary": "cguyan16", "lastMessage": "2022-11-13T23:29:55Z" },
{ "chatId": 44, "destinataryId": "3255fe01-7200-4b88-9967-2fef97c0d992", "destinatary": "bocooney17", "lastMessage": "2022-08-29T19:16:10Z" },
{ "chatId": 45, "destinataryId": "df42a54c-59c3-417d-8dd2-ed5c968b40d6", "destinatary": "dbrowett18", "lastMessage": "2021-05-16T04:29:59Z" },
{ "chatId": 46, "destinataryId": "6b209854-cc2f-4809-b8fd-e6d6070e6569", "destinatary": "peck19", "lastMessage": "2021-07-02T09:26:01Z" },
{ "chatId": 47, "destinataryId": "ed1fbdfd-d97d-4785-8767-3882469da9a5", "destinatary": "iorafferty1a", "lastMessage": "2022-01-31T09:03:11Z" },
{ "chatId": 48, "destinataryId": "5343d277-44a2-4d4e-a22d-2dd3620b889f", "destinatary": "jstedall1b", "lastMessage": "2021-06-20T11:26:41Z" },
{ "chatId": 49, "destinataryId": "1bf474d3-6e69-4f56-a784-5861ec08a5fe", "destinatary": "pvaller1c", "lastMessage": "2022-05-30T20:56:38Z" },
{ "chatId": 50, "destinataryId": "50802164-9100-46d2-b992-9a1052d4ddf6", "destinatary": "lscollan1d", "lastMessage": "2021-01-15T13:13:19Z" },
{ "chatId": 51, "destinataryId": "fcdf4c3a-2607-4ace-a4b3-99c1b42881cb", "destinatary": "emugglestone1e", "lastMessage": "2022-06-05T01:50:44Z" },
{ "chatId": 52, "destinataryId": "9ad4381b-ac65-429b-a6e9-a849aec28ac1", "destinatary": "fnunnerley1f", "lastMessage": "2022-11-17T18:15:12Z" },
{ "chatId": 53, "destinataryId": "cdbdc0a8-a8fd-466c-8a9c-3183469f9172", "destinatary": "clafont1g", "lastMessage": "2022-02-16T18:01:39Z" },
{ "chatId": 54, "destinataryId": "050a733a-d942-4ce8-8655-e5ce73aa7ed2", "destinatary": "obruty1h", "lastMessage": "2022-02-28T22:41:31Z" },
{ "chatId": 55, "destinataryId": "5a83bae9-8c47-4638-a725-4803c67599cd", "destinatary": "bchristophers1i", "lastMessage": "2021-09-25T00:14:27Z" },
{ "chatId": 56, "destinataryId": "97705719-ce11-4634-b205-068f4ede27f4", "destinatary": "ppitsall1j", "lastMessage": "2021-08-15T07:11:25Z" },
{ "chatId": 57, "destinataryId": "d79fc07c-8036-49e7-b2e4-b31cc38478d1", "destinatary": "cwhanstall1k", "lastMessage": "2022-09-12T16:36:34Z" },
{ "chatId": 58, "destinataryId": "242a872b-b992-47c9-84ce-c892f30f2b7c", "destinatary": "dbottomore1l", "lastMessage": "2021-10-25T07:38:06Z" },
{ "chatId": 59, "destinataryId": "984d90b4-b861-40da-81ca-5e6c445e93bf", "destinatary": "cshewen1m", "lastMessage": "2021-02-05T15:15:16Z" },
{ "chatId": 60, "destinataryId": "28298df5-77b9-457e-9552-aafa2f4d1556", "destinatary": "arosenhaus1n", "lastMessage": "2022-12-17T00:59:27Z" },
{ "chatId": 61, "destinataryId": "2a1a92c1-9a01-493c-9cde-e068687b9a0f", "destinatary": "mfryd1o", "lastMessage": "2021-11-16T13:31:14Z" },
{ "chatId": 62, "destinataryId": "5258d22f-e401-4753-909b-41f7d1803648", "destinatary": "dsplaven1p", "lastMessage": "2022-12-24T20:47:20Z" },
{ "chatId": 63, "destinataryId": "412f0e82-5d3e-48e5-8ef4-0d28790309cc", "destinatary": "cohalligan1q", "lastMessage": "2022-07-18T04:39:16Z" },
{ "chatId": 64, "destinataryId": "4b0cb6a8-ce2a-4413-b18d-bc901bcb8adc", "destinatary": "tostick1r", "lastMessage": "2021-04-01T00:42:53Z" },
{ "chatId": 65, "destinataryId": "ee4ba67a-9a11-4f59-8cee-00353f48aaf9", "destinatary": "ireiners1s", "lastMessage": "2022-10-26T14:20:09Z" },
{ "chatId": 66, "destinataryId": "04d0f1ee-5711-4e3e-a9bf-781a26ac7032", "destinatary": "rcuerda1t", "lastMessage": "2021-12-04T08:39:37Z" },
{ "chatId": 67, "destinataryId": "c4252d93-9605-4ae1-aec0-9b8800a41ffa", "destinatary": "jshackell1u", "lastMessage": "2023-01-01T23:39:57Z" },
{ "chatId": 68, "destinataryId": "319c898b-c102-4c13-9769-e79e0967f1b1", "destinatary": "dparslow1v", "lastMessage": "2022-11-20T11:18:18Z" },
{ "chatId": 69, "destinataryId": "76b4c31b-772e-4551-bfb6-5edc6a78a602", "destinatary": "aparidge1w", "lastMessage": "2021-04-05T08:44:31Z" },
{ "chatId": 70, "destinataryId": "c08877fa-ffde-4f2f-ad8a-c506669e4a1d", "destinatary": "slamacraft1x", "lastMessage": "2022-05-05T09:05:00Z" },
{ "chatId": 71, "destinataryId": "c71dece5-4cff-429d-9eac-2a34721eade1", "destinatary": "dcrosetto1y", "lastMessage": "2021-11-22T15:21:36Z" },
{ "chatId": 72, "destinataryId": "b898e6dc-ec2d-4318-9665-8d6eb0008fd1", "destinatary": "rwitherspoon1z", "lastMessage": "2022-04-01T09:53:38Z" },
{ "chatId": 73, "destinataryId": "76f9d145-c767-4853-a563-770c674daa38", "destinatary": "omanterfield20", "lastMessage": "2021-01-10T03:11:32Z" },
{ "chatId": 74, "destinataryId": "51c06786-a23d-466a-8be2-ab2dc854721e", "destinatary": "ipostgate21", "lastMessage": "2022-02-26T12:07:30Z" },
{ "chatId": 75, "destinataryId": "26f351c2-e48c-4b97-9e04-a0a4648598c3", "destinatary": "jbruyett22", "lastMessage": "2022-07-15T11:14:11Z" },
{ "chatId": 76, "destinataryId": "c4af7be6-ff92-4001-8891-375a900213f0", "destinatary": "vbeltzner23", "lastMessage": "2021-05-20T00:26:07Z" },
{ "chatId": 77, "destinataryId": "7c526e95-452a-49cf-86dc-37af18d69a0c", "destinatary": "avaneeden24", "lastMessage": "2023-01-01T17:06:55Z" },
{ "chatId": 78, "destinataryId": "98d07095-f471-45d7-bc7d-94fc1ab0e998", "destinatary": "kmotton25", "lastMessage": "2021-09-27T11:49:03Z" },
{ "chatId": 79, "destinataryId": "e7ee1367-bc4c-43fb-a1cc-cb19c9d25b82", "destinatary": "nlucock26", "lastMessage": "2022-02-24T17:39:35Z" },
{ "chatId": 80, "destinataryId": "c373e2b0-a65b-4612-9e0e-4600d27b68a5", "destinatary": "pcopnell27", "lastMessage": "2021-07-22T18:55:43Z" },
{ "chatId": 81, "destinataryId": "b65ab95e-78f5-4427-8a4c-548feaca25f6", "destinatary": "jkimmings28", "lastMessage": "2022-05-01T23:16:39Z" },
{ "chatId": 82, "destinataryId": "e5bf77f9-6e17-4060-9457-bbecc5deac85", "destinatary": "dfeatherstonehaugh29", "lastMessage": "2021-05-19T11:24:40Z" },
{ "chatId": 83, "destinataryId": "4e56bbef-a8f7-40e0-a27f-b9a50d3439c7", "destinatary": "ctuttle2a", "lastMessage": "2022-11-27T11:14:55Z" },
{ "chatId": 84, "destinataryId": "c6f48f5c-24b1-4782-8bde-f733ecc2fd28", "destinatary": "afolonin2b", "lastMessage": "2022-08-12T23:31:28Z" },
{ "chatId": 85, "destinataryId": "026036a8-697d-4143-ad2e-63d22c7a79eb", "destinatary": "cshorrock2c", "lastMessage": "2021-08-12T23:08:45Z" },
{ "chatId": 86, "destinataryId": "e979d236-e8da-4502-824d-20f1d7ea3574", "destinatary": "ycollip2d", "lastMessage": "2021-08-05T20:03:11Z" },
{ "chatId": 87, "destinataryId": "ff24163a-d7b5-4d81-8bc7-dbd4dbe15ab1", "destinatary": "chaslum2e", "lastMessage": "2021-12-12T06:27:57Z" },
{ "chatId": 88, "destinataryId": "eefa1215-5483-4a77-9e98-bc4146738f4a", "destinatary": "pcoryndon2f", "lastMessage": "2021-01-08T13:02:07Z" },
{ "chatId": 89, "destinataryId": "8ca1f7b6-55a0-4ca9-a4ac-d3b3c2fd0a65", "destinatary": "rorris2g", "lastMessage": "2021-03-22T23:50:07Z" },
{ "chatId": 90, "destinataryId": "669f0141-b637-450a-985e-07f165e5cba9", "destinatary": "bbazoche2h", "lastMessage": "2021-06-04T16:37:58Z" },
{ "chatId": 91, "destinataryId": "d9a8401e-4d28-44e4-8204-3b75c9c35d9c", "destinatary": "vpiniur2i", "lastMessage": "2021-08-29T03:47:29Z" },
{ "chatId": 92, "destinataryId": "21ede8e6-43e6-4c09-8a40-b9f394a64b3a", "destinatary": "pprigg2j", "lastMessage": "2022-10-14T03:36:05Z" },
{ "chatId": 93, "destinataryId": "014dce24-289e-4516-9e12-d848996a8d44", "destinatary": "mkahane2k", "lastMessage": "2022-09-23T20:07:06Z" },
{ "chatId": 94, "destinataryId": "0f7d9169-5aed-4ca7-807d-01097ce214e2", "destinatary": "bdrinkel2l", "lastMessage": "2021-01-12T20:28:42Z" },
{ "chatId": 95, "destinataryId": "e7a6e533-23eb-4798-90d8-aa0c6a4c31ee", "destinatary": "apuddin2m", "lastMessage": "2021-10-29T08:52:56Z" },
{ "chatId": 96, "destinataryId": "c912f85b-e5f2-45db-9c0e-d8212721206a", "destinatary": "vbrouard2n", "lastMessage": "2022-06-04T01:50:21Z" },
{ "chatId": 97, "destinataryId": "eccee509-f761-4c70-a086-c239028fd17b", "destinatary": "sviney2o", "lastMessage": "2022-07-21T07:00:49Z" },
{ "chatId": 98, "destinataryId": "707113ce-f439-46c1-bde5-b88a87a19510", "destinatary": "kjugging2p", "lastMessage": "2021-06-22T00:18:36Z" },
{ "chatId": 99, "destinataryId": "cd43f040-6bcc-42ca-87ee-65d885a9543e", "destinatary": "trimmer2q", "lastMessage": "2021-12-22T02:04:16Z" },
{ "chatId": 100, "destinataryId": "e5d3eb34-2fec-4207-b397-0b5593f3520a", "destinatary": "cwormstone2r", "lastMessage": "2021-08-28T02:00:44Z" }]

let config = {
	username: "developer",
	photo: "",
}

app.get('/chats', (req, res) => {
	res.set('Access-Control-Allow-Origin', '*');
	res.json(chats)
});

app.get('/config', (req, res) => {
	res.set('Access-Control-Allow-Origin', '*');
	res.json(config)
});

app.post('/config', jsonParser, (req, res) => {
	config = { username: req.body.username, photo: req.body.photo }

	res.set('Access-Control-Allow-Origin', '*');
	res.send("ok")
});

app.listen(port, () => {
	console.log(`ChatAPI running on port: ${port}`)
})