import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import ConfigurationView from './views/configuration/ConfigurationView';
import ChatView from './views/chat/ChatView';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<ChatView />} />
        <Route path='/configurations' element={<ConfigurationView />} />
      </Routes>
    </BrowserRouter >
  )
}

export default App
