import { Fragment } from "react/jsx-runtime";
import './MessageComponent.scss';

type Props = {
    text: string,
    backgroundColor: string,
    marginLeft?: string,
    marginRight?: string
}

const MessageComponent = (props: Props) => {
    return (
        <Fragment>
            <div 
                className="message-component" 
                style={{
                    backgroundColor: props.backgroundColor,
                    marginLeft: props.marginLeft,
                    marginRight: props.marginRight
                }}
            >
                {props.text}
            </div>
        </Fragment>
    );
}

export default MessageComponent;