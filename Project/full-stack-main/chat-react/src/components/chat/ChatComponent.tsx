import { Fragment } from "react/jsx-runtime";
import Chat from "../../models/Chat";
import './ChatComponent.scss';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircleUser } from "@fortawesome/free-regular-svg-icons";
import moment from "moment";

type Props = {
    chat: Chat,
    callback: Function
}

const ChatComponent = (props: Props) => {
    return (
        <Fragment>
            <div className="chat-component" onClick={() => props.callback()}>
                <div className="chat-component-avatar">
                    <FontAwesomeIcon icon={faCircleUser} />
                </div>

                <div className="chat-component-username">
                    {props.chat.destinatary}
                </div>

                <div className="chat-component-datetime">
                    {moment(props.chat.lastMessage).format('MM/DD/YYYY')}
                    <br />
                    {moment(props.chat.lastMessage).format('HH:MM A')}
                </div>
            </div>
        </Fragment>
    );
}

export default ChatComponent;