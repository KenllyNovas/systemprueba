import axios from "axios";
import Chat from "../models/Chat";

class ChatService {
    static async getAll(): Promise<Array<Chat>> {
        return await (() => {
            return new Promise((resolve) => {
                axios({
                    method: 'get',
                    url: 'http://localhost:3000/chats',
                    responseType: 'json'
                })
                    .then((response) => {
                        resolve(response.data);
                    });
            });
        })();
    }
}

export default ChatService;