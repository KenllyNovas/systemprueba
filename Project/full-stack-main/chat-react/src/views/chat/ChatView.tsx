import { useCallback, useEffect, useState } from "react";
import { Fragment } from "react/jsx-runtime"
import ChatService from "../../services/ChatService";
import ChatComponent from "../../components/chat/ChatComponent";
import Chat from "../../models/Chat";
import './ChatView.scss';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircleUser, faPaperPlane } from "@fortawesome/free-regular-svg-icons";
import { faEllipsisVertical, faPlus } from "@fortawesome/free-solid-svg-icons";
import MessageComponent from "../../components/message/MessageComponent";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons/faChevronLeft";
import { Link } from "react-router-dom";

const ChatListView = () => {
    const [chats, setChats] = useState<Array<Chat>>([]);
    const [shouldHideChats, setShouldHideChats] = useState<boolean>(true);

    useEffect(() => {
        ChatService.getAll().then((result: Array<Chat>) => {
            setChats(result);
        });
    }, []);

    const hideChats = useCallback(() => {
        setShouldHideChats(true);
    }, []);

    const loadChat = useCallback(() => {
        setShouldHideChats(false)
    }, []);

    return (
        <Fragment>
            <div className="chat-list-view-container">
                <div className="chat-list-view-left">
                    <div className="chat-list-view-header">
                        <div className="chat-list-view-avatar">
                            <FontAwesomeIcon icon={faCircleUser} />
                        </div>

                        <div className="chat-list-view-user">
                            Jhon Doe
                        </div>

                        <Link className="chat-list-view-dots" to="/configurations">
                            <FontAwesomeIcon icon={faEllipsisVertical} />
                        </Link>
                    </div>

                    <div className="chat-list-view-body">
                        {
                            chats.map((chat: Chat) => {
                                return (
                                    <ChatComponent key={chat.chatId} chat={chat} callback={() => loadChat()} />
                                );
                            })
                        }
                    </div>

                    <div className="chat-list-view-bottom">
                        <button className="chat-list-view-add">
                            <FontAwesomeIcon icon={faPlus} />
                        </button>
                    </div>
                </div>

                <div className="chat-list-view-right" hidden={shouldHideChats}>
                    <div className="chats-view-header">
                        <button className="chats-view-back" onClick={() => hideChats()}>
                            <FontAwesomeIcon icon={faChevronLeft} />
                        </button>

                        <div className="chats-view-avatar">
                            <FontAwesomeIcon icon={faCircleUser} />
                        </div>

                        <div className="chats-view-user">
                            Jane Doe
                        </div>
                    </div>

                    <div className="chats-view-body">
                        <MessageComponent
                            text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. "
                            backgroundColor="#F4F2FF"
                            marginRight="50px"
                        />

                        <MessageComponent
                            text="Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. "
                            backgroundColor="#e8e6fb"
                            marginLeft="50px"
                        />
                    </div>

                    <div className="chats-view-bottom">
                        <input className="chats-view-input" type="text" placeholder="Write a message here" />

                        <button className="chats-view-send">
                            <FontAwesomeIcon icon={faPaperPlane} />
                        </button>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}

export default ChatListView;