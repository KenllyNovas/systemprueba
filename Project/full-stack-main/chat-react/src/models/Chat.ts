class Chat {
    public chatId: number | null;
    public destinataryId: string | null;
    public destinatary: string | null;
    public lastMessage: string | null;

    constructor(){
        this.chatId = null;
        this.destinataryId = null;
        this.destinatary = null;
        this.lastMessage = null;
    }
}

export default Chat;